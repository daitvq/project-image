import React from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./../firebaseConfig";
import { Link, useNavigate } from "react-router-dom";
import { signOut } from "firebase/auth";
import { toast } from "react-toastify";
import { Button } from "antd";

export default function Navbar() {
  const [user] = useAuthState(auth);
  let navigate = useNavigate();
  const LogOut = async () => {
    try {
      await signOut(auth);
      navigate("/signin");
    } catch (error) {
      toast(error.code, { type: "error" });
    }
  };
  return (
    <nav className="navbar">
      <div className="navbar-nameapp">
        <p>ART GALLERY</p>
        <div>
          <Link className="link-home" to="/">
            Home
          </Link>
          {user ? (
            <Link className="link-profile" to="profile">
              Profile
            </Link>
          ) : (
            <Link className="link-profile" to="signin">
              Login
            </Link>
          )}
        </div>
      </div>
      <div className="navbar-profile">
        {user && (
          <>
            <p className="navbar-wel">Welcome {user.email}</p>
            <Button className="button-logout" type="button" onClick={LogOut}>
              LogOut
            </Button>
          </>
        )}
      </div>
    </nav>
  );
}
