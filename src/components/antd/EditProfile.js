import React from "react";
import { Button, Form, Input, InputNumber, Modal, Upload } from "antd";
import { useState } from "react";
import PropTypes from "prop-types";

const normFile = (e) => {
  console.log("Upload event:", e);

  if (Array.isArray(e)) {
    return e;
  }

  return e?.file;
};

const CollectionCreateForm = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();
  return (
    <Modal
      visible={visible}
      title="Create a new image"
      okText="Edit"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            onCreate(values);
            form.resetFields();
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal" >
        <Form.Item
          name="name"
          label="Name"
          rules={[
            {
              required: true,
              message: "Please input the name of collection!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="age"
          label="Age"
          rules={[
            {
              required: true,
              message: "Please input the age of collection!",
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item
          name="description"
          label="Description"
          rules={[
            {
              required: true,
              message: "Please input the description of collection!",
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          name="avatar"
          label="Avatar"
          valuePropName="file"
          getValueFromEvent={normFile}
        >
          <Upload name="logo" listType="picture" beforeUpload={() => false}>
            <Button>Click to upload</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default function CreateImg(props) {
  const editProfile = props.editProfileFunc;
  const infoUser = props.infoUserProps;
  console.log(infoUser);
  const [visible, setVisible] = useState(false);

  const onCreate = (values) => {
    console.log("values", values);
    editProfile(values);
    setVisible(false);
  };

  return (
    <div>
      <Button
        className="button-edit"
        type="primary"
        onClick={() => {
          setVisible(true);
        }}
      >
        EDIT
      </Button>
      <CollectionCreateForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
}
CreateImg.propTypes = {
  editProfileFunc: PropTypes.func.isRequired,
  infoUserProps: PropTypes.object.isRequired,
};
