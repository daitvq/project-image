import React from "react";
import { Button, Form, Input, Modal, Upload } from "antd";
import { useState } from "react";
import PropTypes from "prop-types";

const normFile = (e) => {
  console.log("Upload event:", e);

  if (Array.isArray(e)) {
    return e;
  }

  return e?.file;
};

const CollectionCreateForm = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();
  return (
    <Modal
      visible={visible}
      title="Create a new image"
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            onCreate(values);
            form.resetFields();
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please input the title of collection!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item 
        name="description" 
        label="Description"
        rules={[
          {
            required: true,
            message: "Please input the description of collection!",
          },
        ]}>
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          name="image"
          label="Image"
          valuePropName="file"
          getValueFromEvent={normFile}
          rules={[
            {
              required: true,
              message: "Please input the image of collection!",
            },
          ]}>
          <Upload name="logo" listType="picture" beforeUpload={() => false}>
            <Button>Click to upload</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default function CreateImg(props) {
  const addImg = props.addImgFunc;

  const [visible, setVisible] = useState(false);

  const onCreate = (values) => {
    console.log("values", values);
    addImg(values);
    setVisible(false);
  };

  return (
    <div>
      <Button
        className="button-add"
        type="primary"
        onClick={() => {
          setVisible(true);
        }}
      >
        CREATE IMAGE
      </Button>
      <span>Click to add photo</span>
      <CollectionCreateForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
}
CreateImg.propTypes = {
  addImgFunc: PropTypes.func.isRequired,
};
