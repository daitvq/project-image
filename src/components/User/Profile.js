import React, { useEffect, useState } from "react";
import { Row, Col, Avatar } from "antd";
import {
  collection,
  doc,
  getDocs,
  onSnapshot,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import { auth, db, storage } from "../../firebaseConfig";
import EditProfile from "../antd/EditProfile";
import { v4 } from "uuid";
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytesResumable,
} from "firebase/storage";
import { useAuthState } from "react-firebase-hooks/auth";

export default function Profile() {
  const [profile, setProfile] = useState([]);
  const [oldImg, setOldImg] = useState("");
  const [user] = useAuthState(auth);
  let emailPro = user.email;

  useEffect(() => {
    const profileRef = collection(db, "Users");
    const q = query(profileRef, where("email", "==", emailPro));
    onSnapshot(q, (snapshot) => {
      let pro = [];
      snapshot.docs.forEach((doc) => {
        pro.push({ ...doc.data(), id: doc.id });
        setOldImg(pro[0][`avatar`]);
        setProfile(pro);
      });
    });
  }, [emailPro]);

  const editProfile = async (value) => {
    if (!value.name || !value.age || !value.description) {
      alert("Please fill all the fields");
      return;
    }
    const q = query(collection(db, "Users"), where("email", "==", emailPro));
    const querySnapshot = await getDocs(q);
    let docID = "";
    querySnapshot.forEach((doc) => {
      docID = doc.id;
    });
    const user = await doc(db, "Users", docID);

    const storageRef = await ref(
      storage,
      `/images/${value.avatar.name + v4()}`
    );
    uploadBytesResumable(storageRef, value.avatar).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        if (
          oldImg ===
          "https://ibiettuot.com/wp-content/uploads/2021/10/avatar-mac-dinh.png"
        ) {
          updateDoc(user, {
            name: value.name,
            age: value.age,
            description: value.description,
            avatar: url,
          });
        } else {
          const storageRef = ref(storage, oldImg);
          deleteObject(storageRef);
          updateDoc(user, {
            name: value.name,
            age: value.age,
            description: value.description,
            avatar: url,
          });
        }
      });
    });
  };

  return (
    <div>
      {profile.map(function (p) {
        return (
          <div className="form-profile" key={p.id}>
            <p className="title-profile">PROFILE</p>
            <Row>
              <Col span={16} push={8}>
                <div className="form-profile-view">
                  <div className="info-profile-title">
                    <span>Name:</span>
                    <p>{p.name}</p>
                  </div>
                  <div className="info-profile-title">
                    <span>Age:</span>
                    <p>{p.age}</p>
                  </div>
                  <div className="info-profile-title">
                    <span>Email:</span>
                    <p>{p.email}</p>
                  </div>
                  <div className="info-profile-desc">
                    <span>Description:</span>
                    <p className="info-profile-desc-in">{p.description}</p>
                  </div>
                  <EditProfile
                    editProfileFunc={editProfile}
                    infoUserProps={p}
                  />
                </div>
              </Col>
              <Col span={8} pull={16}>
                <Avatar className="avatar-prof" size={190} src={p.avatar} />
              </Col>
            </Row>
          </div>
        );
      })}
    </div>
  );
}
