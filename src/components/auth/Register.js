import React from "react";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth, db } from "./../../firebaseConfig";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { Button, Form, Input } from "antd";
import { addDoc, collection } from "firebase/firestore";

export default function Register() {
  let navigate = useNavigate();

  const onFinish = async (value) => {
    try {
      await createUserWithEmailAndPassword(auth, value.email, value.password);
      const userRef = collection(db, "Users");
      addDoc(userRef, {
        email: value.email,
        name: "",
        age: "",
        avatar: "https://ibiettuot.com/wp-content/uploads/2021/10/avatar-mac-dinh.png",
        description: ""
      })
        .then(() => {
          navigate("/");
        })
        .catch((err) => {
          toast("Error register User", { type: "error" });
        });
    } catch (error) {
      toast(error.code, { type: "error" });
    }
  };
  return (
    <div className="form-logrer">
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <p className="title-logres">REGISTER</p>
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your Email!",
            },
          ]}
        >
          <Input type="email" placeholder="Username" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
        >
          <Input type="password" placeholder="Password" />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Register
          </Button>
          Or <a href="./signin">Login now!</a>
        </Form.Item>
      </Form>
    </div>
  );
}
