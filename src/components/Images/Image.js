import { Card, List } from "antd";
import { collection, onSnapshot, orderBy, query } from "firebase/firestore";
import React, { useEffect, useState } from "react";
import { db } from "../../firebaseConfig";
import DelImage from "./DelImage";

export default function Image() {
  const [image, setImage] = useState([]);

  useEffect(() => {
    const imageRef = collection(db, "Images");
    const q = query(imageRef, orderBy("title", "asc"));
    onSnapshot(q, (snapshot) => {
      const img = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      setImage(img);
    });
  }, []);
  return (
    <div className="view-image">
      {image.length === 0 ? (
        <p>No image found</p>
      ) : (
        <List
          grid={{
            gutter: 12,
            column: 3,
          }}
          dataSource={image}
          renderItem={(item) => (
            <List.Item>
              <Card
                key={item.id}
                hoverable
                style={{
                  width: 330,
                  margin: 30,
                }}
                cover={<img className="image" alt="example" src={item.imageUrl} />}
              >
                <div className="info-image">
                  <p className="info-image-title">Title: {item.title}</p>
                  <span className="info-image-span">Description:</span> 
                  <p className="info-image-desc">{item.description}</p>
                  <DelImage id={item.id} imageUrl={item.imageUrl} />
                </div>
              </Card>
            </List.Item>
          )}
        />
      )}
    </div>
  );
}
