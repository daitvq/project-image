import { deleteDoc, doc } from "firebase/firestore";
import React from "react";
import { db, storage } from "../../firebaseConfig";
import { toast } from "react-toastify";
import { deleteObject, ref } from "firebase/storage";

export default function DelImage({ id, imageUrl }) {
  const handleDelete = async () => {
    if (window.confirm("Are you sure you want to delete this image?")) {
      try {
        await deleteDoc(doc(db, "Images", id));
        const storageRef = ref(storage, imageUrl);
        await deleteObject(storageRef);
      } catch (error) {
        toast("Error deleting image", { type: "error" });
      }
    }
  };
  return (
    <div>
      <button
        className="button-del"
        onClick={handleDelete}
        style={{ cursor: "pointer" }}
      >
        Delete
      </button>
    </div>
  );
}
