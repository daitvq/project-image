import React from "react";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { storage, db } from "../../firebaseConfig";
import { collection, addDoc } from "firebase/firestore";
import { toast } from "react-toastify";
import { v4 } from "uuid";
import CreateImg from "../antd/CreateImg";

export default function AddImage() {
  const addImg = async (values) => {
    if (!values.title || !values.description || !values.image) {
      alert("Please fill all the fields");
      return;
    }
    const storageRef = await ref(
      storage,
      `/images/${values.image.name + v4()}`
    );
    uploadBytesResumable(storageRef, values.image).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        const imageRef = collection(db, "Images");
        addDoc(imageRef, {
          title: values.title,
          description: values.description,
          imageUrl: url,
        })
          .then(() => {
            toast("Article added successfully", { type: "success" });
          })
          .catch((err) => {
            toast("Error adding article", { type: "error" });
          });
      });
    });
  };

  return (
    <>
      <CreateImg addImgFunc={addImg} />
    </>
  );
}
