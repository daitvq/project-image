import React from "react";
import AddImage from "./AddImage";
import Image from "./Image";

export default function ListImage() {
  return (
    <div className="form-image">
      <AddImage />
      <Image />
    </div>
  );
}
