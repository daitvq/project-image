import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDkZB-gskJHZdnMNwAivaMtnVqWf862x1A",
  authDomain: "fir-img-73f92.firebaseapp.com",
  projectId: "fir-img-73f92",
  storageBucket: "fir-img-73f92.appspot.com",
  messagingSenderId: "571638745356",
  appId: "1:571638745356:web:397a38c7cfd506cf9c2129",
};

const app = initializeApp(firebaseConfig);

export const storage = getStorage(app);
export const db = getFirestore(app);
export const auth = getAuth(app);
