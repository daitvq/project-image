import "./App.css";
import "antd/dist/antd.min.css";
import { Route, Routes } from "react-router-dom";
import Profile from "./components/User/Profile";
import ListImage from "./components/Images/ListImage";
import Navbar from "./components/Navbar";
import { Layout } from "antd";
import { Content, Footer, Header } from "antd/lib/layout/layout";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";

export default function App() {
  return (
    <div className="container">
      <Layout>
        <Header>
          <Navbar />
        </Header>
        <Content>
          <Routes>
            <Route path="/register" element={<Register />} />
            <Route path="/signin" element={<Login/>} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/" element={<ListImage />} />
          </Routes>
        </Content>
        <Footer style={{minHeight: 90}}></Footer>
      </Layout>
    </div>
  );
}
